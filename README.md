# ckanext-newextension

This is the README of the Theming Extension of the ZLE Repository page.
The Theming Extension is part of CKAN and is adapted for the usage of the [Zukunftslabor Energie](https://www.zdin.de/zukunftslabore/energie). For further references of CKAN please refer to the official [CKAN Documentation](https://docs.ckan.org/en/latest/).


## Requirements
Compatibility with core CKAN versions:

 CKAN 2.10.0 with ckan-docker


## Installation
The installation has two possibilities: The installation for using in the development enviroment of CKAN or as finished product on a deployed CKAN page.

1. If you want to extend the Theming Extension, then you have to install the Repository into your own `ckan-docker` instance <br>
```bash
cd $CKAN_VENV/src/
pip install -e "https://gitlab.com/zdin-zle/zle-platform/repository/zle-theme.git"
```

2. If you just want to use this extension you only need to add the extension into your Dockerfile
```bash
RUN  pip3 install -e 'git+https://gitlab.com/zdin-zle/zle-platform/repository/zle-theme.git@main#egg=ckanext-newextension'
```


For furthe help, please refer to the official [CKAN Documentation](https://docs.ckan.org/en/2.9/maintaining/installing/install-from-docker-compose.html#add-extensions)