import ckan.plugins.toolkit as tk
import ckanext.newextension.logic.schema as schema


@tk.side_effect_free
def newextension_get_sum(context, data_dict):
    tk.check_access(
        "newextension_get_sum", context, data_dict)
    data, errors = tk.navl_validate(
        data_dict, schema.newextension_get_sum(), context)

    if errors:
        raise tk.ValidationError(errors)

    return {
        "left": data["left"],
        "right": data["right"],
        "sum": data["left"] + data["right"]
    }


def get_actions():
    return {
        'newextension_get_sum': newextension_get_sum,
    }
